import prismadb from "@/lib/prismadb";
import React from "react";

interface CompanionPageProps {
  params: {
    companionId: string;
  };
}
const CompanionPage = async ({ params }: CompanionPageProps) => {
  // TODO: CHECK SUBSCRIPTION

  // this fetch is for make sure tha should we use crate page or edit page
  const companion = await prismadb.companion.findUnique({
    where: {
      id: params.companionId,
    },
  });
  const categories = await prismadb.companion.findMany();
  return <div>comapnion</div>;
};
export default CompanionPage;
