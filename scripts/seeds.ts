const { PrismaClient } = require("@prisma/client");

const db = new PrismaClient();

async function main() {
  try {
    await db.Category.createMany({
      data: [
        { name: "Famous People" },
        { name: "Movies & TV" },
        { name: "Musicians" },
        { name: "Games" },
        { name: "Animals" },
        { name: "Philosopher" },
        { name: "Scientist" },
      ],
    });
  } catch (error) {
    console.error("error sending categories", error);
  } finally {
    await db.$disconnect;
  }
}

main();
