"use client";
import React from "react";
import { Home, Plus, Settings } from "lucide-react";
import { HtmlContext } from "next/dist/server/future/route-modules/app-page/vendored/contexts/entrypoints";
import { usePathname, useRouter } from "next/navigation";
import { cn } from "@/lib/utils";
const sidebar = () => {
  const pathName = usePathname();
  const router = useRouter();
  const routes = [
    {
      icon: Home,
      href: "/",
      label: "Home",
      pro: false,
    },
    {
      icon: Plus,
      href: "/companion/new",
      label: "Create",
      pro: true,
    },

    {
      icon: Settings,
      href: "/settings",
      label: "Settings",
      pro: false,
    },
  ];
  const onNavigate = (pro: boolean, url: string): any => {
    return router.push(url);
  };
  return (
    <div className=" space-y-4  flex-coloumn h-full text-primary bg-secondary">
      <div className=" p-3 flex flex-1 justify-center">
        <div className="space-y-2">
          {routes.map((route): any => {
            return (
              <div
                onClick={() => {
                  onNavigate(route.pro, route.href);
                }}
                key={route.href}
                className={cn(
                  "text-muted-foreground text-xs group flex p-3 w-full justify-start font-medium cursor-pointer hover:text-primary hover:bg-primary/10 rounded lg transition",
                  pathName === route.href && "bg-primary/10 text-primary"
                )}
              >
                <div className=" flex flex-col gap-y-2 items-center flex-1">
                  <route.icon className="w-5 h-5" />
                  {route.label}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default sidebar;
