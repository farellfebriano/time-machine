"use client";
import React, { ChangeEventHandler, useEffect, useState } from "react";
import { Input } from "@/components/ui/input";
import { Search } from "lucide-react";
import { useRouter, useSearchParams } from "next/navigation";
import useDebounce from "@/hooks/use-debounce";
import qs from "query-string";

const SearchInput = () => {
  const router = useRouter();
  const searchParams = useSearchParams();

  // where we going to fetch the category
  const categoryId = searchParams.get("categoryId");
  const name = searchParams.get("name");

  const [value, setValue] = useState(name || "");
  const debouncedValue = useDebounce<string>(value, 500);

  const onChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setValue(e.target.value);
  };

  useEffect(() => {
    const query = {
      name: debouncedValue,
      categoryId: categoryId,
    };

    /*
    queryString.parse(location.search) is used to parse the query parameters
    from the current URL. Adjust the import and usage based on your specific
    requirements and use case in your Next.js project.
    */

    const url = qs.stringifyUrl(
      {
        // this is where we can get the current href
        url: window.location.href,
        query,
      },
      { skipEmptyString: true, skipNull: true }
    );

    router.push(url);
  }, [debouncedValue, router, categoryId]);

  return (
    <div className="relative">
      <Search className=" absolute h-4 w-4 top-3 left-4 text-muted-foreground" />
      <Input
        className=" pl-10 bg-primary/10 "
        placeholder="Search . . ."
        onChange={onChange}
        value={value}
      />
    </div>
  );
};

export default SearchInput;

/*
when Input is onchange, it will start the onchange function that will set the value
use it as a parameter in debounce hook. it will keep it 0.5 second and if it still change it it will
always update the time. When the value is not changing anymore than it would return a
the value.

second, it will pu the value into stringifyUrl for tailoring it with the current http rputes and
i pushes to the "router.push(url);"
 */
