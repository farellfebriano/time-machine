"use client";
import React from "react";
import { Menu, Sparkles } from "lucide-react";
import Sidebar from "./sidebar";
import { Sheet, SheetContent, SheetTrigger } from "@/components/ui/sheet";

export default function MobileSidebar() {
  return (
    <Sheet>
      <SheetTrigger className="block md:hidden">
        <Menu />
      </SheetTrigger>
      <SheetContent side="left" className="p-0 pt-10 bg-secondary w-32">
        <Sidebar />
      </SheetContent>
    </Sheet>
  );
}
