"use client";
import React from "react";
import { Category } from "@prisma/client";
import { cn } from "@/lib/utils";
import Router from "next/router";
import { useRouter, useSearchParams } from "next/navigation";
import qs from "query-string";
import { Search } from "lucide-react";

interface CategoryProps {
  data: Category[];
}

export default function Categories({ data }: CategoryProps) {
  const router = useRouter();
  const searchParams = useSearchParams();
  /*this will get the id in the search bar that will use for the css.
  So the we can make the button difernt baggrunf when it is currently selected*/
  const categoryId = searchParams.get("categoryId");

  const onClick = (id: string | undefined) => {
    const query = { categoryId: id };

    const url = qs.stringifyUrl(
      {
        url: window.location.href,
        query,
      },
      { skipNull: true }
    );
    router.push(url);
  };

  return (
    <div className="w-full overflow-x-auto space-x-2 flex p-1">
      <button
        onClick={undefined}
        className={cn(
          "flex items-center text-center text-xs md:text-sm px-2 md:px-4 py-2 md:py-3 rounded-md bg-primary/10 hover:opacity-75 transition",
          categoryId ? "bg-primary/10" : "bg-primary/25"
        )}
      >
        Newest
      </button>
      {data.map((category, index) => {
        return (
          <button
            onClick={() => {
              onClick(category.id);
            }}
            key={index}
            className={cn(
              "flex items-center text-center text-xs md:text-sm px-2 md:px-4 py-2 md:py-3 rounded-md bg-primary/10 hover:opacity-75 transition",
              categoryId === category.id ? "bg-primary/25" : "bg-primary/10"
            )}
          >
            {category.name}
          </button>
        );
      })}
    </div>
  );
}
