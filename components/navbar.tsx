"use client";

import React from "react";
import { Menu, Sparkles } from "lucide-react";
import Link from "next/link";
import { Poppins } from "next/font/google";
import { UserButton } from "@clerk/nextjs";
import ModeToggle from "@/components/mode-toggle";
import MobileSidebar from "@/components/mobile-sidebar";

import { cn } from "@/lib/utils";
import { Button } from "./ui/button";

const font = Poppins({
  weight: "600",
  subsets: ["latin"],
});

export default function navbar() {
  return (
    <div className="fixed w-full z-50 flex justify-between items-center py-2 border-b border-primary/10 bg-secondary px-10 h-16">
      <div className="flex items-center">
        <MobileSidebar />
        <Link href="/">
          <h1
            className={cn(
              " hidden  md:block text-xl md:text-3xl font-bold text-primary",
              font.className
            )}
          >
            Time Machine
          </h1>
        </Link>
      </div>
      <div className="flex item-center gap-x-3">
        <Button className=" gap-1" size={"sm"} variant={"premium"}>
          upgrade
          <Sparkles className=" h-4 w-4 fill-yellow-300" />
        </Button>
        <ModeToggle />
        <div className=" my-auto">
          <UserButton />
        </div>
      </div>
    </div>
  );
}
