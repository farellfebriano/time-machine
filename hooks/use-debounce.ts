import React from "react";
import { useEffect, useState } from "react";

// so basically wertime the value change it willl reset the timer and set a timer for then it return the debounce value
export default function useDebounce<T>(value: T, delay?: number): T {
  const [debounceValue, setDebounceValue] = useState<T>(value);

  useEffect(() => {
    const timer = setTimeout(() => {
      setDebounceValue(value);
    }, delay || 500);

    return () => {
      clearTimeout(timer);
    };
  }, [value, delay]);
  return debounceValue;
}

/*
NOTE

what is this?
-this is a custom hook so then it give a delay when a user wants to search in serach bar



*/
